.. openreussiteeducative documentation master file

=======================================
openRéussiteÉducative 1.0 documentation
=======================================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


openRéussiteÉducative a pour objet de gérer le programme de réussite éducative. http://www.openmairie.org/catalogue/openreussiteeducative


Vous trouverez dans ce document :

- les principes d'ergonomie

- l'utilisation d'openReussite_Educative

- le paramétrage


Il reste à compléter les chapitres suivants (dans une prochaine édition)


- les widgets


Ce document a pour but de guider les utilisateurs et les développeurs dans la prise en main du projet. Bonne lecture et n'hésitez pas à venir discuter du projet avec la communauté à l’adresse suivante : https://communaute.openmairie.org/c/autres-applications-openmairie


Ergonomie
=========

.. toctree::

   ergonomie/index.rst


Utilisation
===========

.. toctree::

   utilisation/index.rst


Paramétrage
===========

.. toctree::

   parametrage/index.rst


Tableaux de bord
================

.. toctree::

   widget/index.rst


Bibliographie
=============

* http://www.openmairie.org/



Contributeurs
=============

(par ordre alphabétique)

* François Raynaud

