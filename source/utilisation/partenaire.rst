.. _partenaire:

##########
Partenaire
##########

Saisie des partenaires
======================

Il est possible de modifier les partenaires dans le menu application -> partenaire

.. image:: tab_partenaire.png

En appuyant sur modification :

.. image:: form_partenaire.png






Les interventions 
=================

Il est possible de visualiser les interventions pour le partenaire concerné
dans le sous formulaire intervention

.. image:: tab_intervention_partenaire.png


