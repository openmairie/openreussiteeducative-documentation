.. _intervenant:

###########
Intervenant
###########

Les intervenants interviennent lors d'évenement

Saisie des intervenants
=======================

Il est possible de modifier les intervenants dans le menu application -> intervenant

.. image:: tab_intervenant.png

En appuyant sur modification :

.. image:: form_intervenant.png




Les événements 
==============

Il est possible de visualiser  les evenements pour l'intervenant concerné
dans le sous formulaire evenement

.. image:: tab_evenement_intervenant.png



