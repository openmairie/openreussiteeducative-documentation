.. _dossier:

########
Dossiers
########

Saisie des dossiers
===================

Il est possible de modifier les dossiers dans le menu application -> dossier

.. image:: tab_dossier.png

En appuyant sur

.. image:: csv.png

vous transferez le dossier sur tableur (en fonction de la recherche simple)

Vous pouvez alors faire les statistiques (tableau croisé dynamique) et les graphes.


En appuyant sur modification :

.. image:: form_dossier.png

Entrez le début de la voie et eventuellement le numéro et en appuyant sur

.. image:: adresse_postale.png

vous accédez aux adresses postales

Si celle ci existe le dossier est géolocalisé automatiquement.

Il est possible d accéder à la géolocalisation du dossier (utilisation de la table adresse_postale)

.. image:: sig_dossier.png


Les referents 
=============

Il est possible de saisir d' autres referents pour la dossier concernée
dans le sous formulaire referent

.. image:: tab_autre_referent.png

En appuyant sur modification :

.. image:: form_autre_referent.png

La composition familiale 
========================

Il est possible de completer la composition familiale pour la dossier concernée
dans le sous formulaire composition_familiale

.. image:: tab_composition_familiale.png

En appuyant sur modification :

.. image:: form_composition_familiale.png

La critères de repérage
=======================


les critére de repérage sont saisis  pour la dossier concernée
dans le sous formulaire critere reperage

.. image:: tab_critere_reperage.png

En appuyant sur modification :

.. image:: form_critere_reperage.png

Les actions du dossier 
======================

Il est possible de saisir les actions pour la dossier concernée
dans le sous formulaire dossier_action

.. image:: tab_dossier_action.png

En appuyant sur modification :

.. image:: form_dossier_action.png

Les événements 
==============

Il est possible de completer par les evenements pour la dossier concernée
dans le sous formulaire evenement

.. image:: tab_evenement.png

En appuyant sur modification :

.. image:: form_evenement.png

Les interventions 
=================

Il est possible de completer les interventions pour la dossier concernée
dans le sous formulaire intervention

.. image:: tab_intervention.png

En appuyant sur modification :

.. image:: form_intervention.png
