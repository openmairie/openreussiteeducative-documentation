.. _adresse_postale:

###############
adresse_postale
###############

Saisie des adresse_postales
===========================

Il est possible de modifier les adresse_postales dans le menu application -> adresse_postale

.. image:: tab_adresse_postale.png

En appuyant sur modification :

.. image:: form_adresse_postale.png

Visualisation de la position géographique 

.. image:: sig_adresse_postale.png







