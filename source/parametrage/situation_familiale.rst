.. _situation_familiale:

######################
La situation familiale
######################

Saisie des situation_familiales
===============================

Il est possible de modifier les situation_familiales dans le menu application -> situation_familiale

.. image:: tab_situation_familiale.png

En appuyant sur modification :

.. image:: form_situation_familiale.png







