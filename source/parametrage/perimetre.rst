.. _perimetre:

##########
Périmètres
##########


Pour les statistiques par périmètre

Saisie des perimetres
=====================

Il est possible de modifier les perimetres dans le menu application -> perimetre

.. image:: tab_perimetre.png

En appuyant sur modification :

.. image:: form_perimetre.png

En appuyant sur le bouton position :

.. image:: sig_perimetre.png





