.. _referent_familial:

#################
Referent familial
#################

Saisie des referents
====================

Il est possible de modifier les partenaires dans le menu paramétrage -> referent familial

.. image:: tab_referent_familial.png

En appuyant sur modification :

.. image:: form_referent_familial.png





