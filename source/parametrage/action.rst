.. _action:

######
Action
######


Saisie des actions
=====================

Il est possible de modifier les actions dans le menu parametrage -> action

.. image:: tab_action.png

En appuyant sur modification :

.. image:: form_action.png



Les dossiers 
============

Il est possible de visualiser les dossiers pour l' action concerné
dans le sous formulaire intervention

.. image:: tab_dossier_action_action.png


