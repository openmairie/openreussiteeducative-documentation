.. _om_parametre:

============
om_parametre
============


Les om_parametres sont accessibles dans le menu administration -> om_parametre

Ils sont initialisés pour chaque organisme / collectivité.


Les paramètres suivants sont à initialiser

Paramètres généraux
===================


- option_localisation : sig_interne

- registre : forme du registre

- ville


