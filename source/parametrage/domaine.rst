.. _domaine:

#######
Domaine
#######

Saisie des domaines
=====================

Il est possible de modifier les domaines dans le menu paramètrage -> domaine

.. image:: tab_domaine.png

En appuyant sur modification :

.. image:: form_domaine.png


Les objectifs
=============

Il est possible de visualiser les objectifs pour le domaine concerné
dans le sous formulaire objectif

.. image:: tab_objectif_domaine.png

Entrez dans le formulaire pour modifier

.. image:: form_objectif_domaine.png