.. _quartier:

#########
Quartiers
#########

Pour les statistiques géographiques

Saisie des quartiers
=====================

Il est possible de modifier les quartiers dans le menu parametrage -> quartier

.. image:: tab_quartier.png

En appuyant sur modification :

.. image:: form_quartier.png

En appuyant sur le bouton position :

.. image:: sig_quartier.png




