.. _motif:

#####
Motif
#####

Saisie des motifs
=====================

Il est possible de modifier les motifs dans le menu paramétrage -> motif

.. image:: tab_motif.png

En appuyant sur modification :

.. image:: form_motif.png






Les dossiers 
============

Il est possible de visualiser les dossiers pour le motif concerné
dans le sous formulaire dossier

.. image:: tab_dossier_motif.png


