.. _etablissement:

######################
Etablissement scolaire
######################

Saisie des etablissements
=========================

Il est possible de modifier les etablissements dans le menu application -> etablissement

.. image:: tab_etablissement.png


En appuyant sur modification :

.. image:: form_etablissement.png


Entrez le début de la voie et eventuellement le numéro et en appuyant sur

.. image:: adresse_postale.png

vous accédez aux adresses postales

Si celle ci existe l'etablissement est géolocalisé automatiquement.

Visualisation de la position géographique (d'après l adresse postale)

.. image:: sig_etablissement.png



Les dossiers
============

Il est possible de visualiser les dossiers pour l'etablissement concerné
dans le sous formulaire établissement

.. image:: tab_dossier_etablissement.png


