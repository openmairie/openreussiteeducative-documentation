.. _civilite:

########
Civilite
########

Saisie des civilites
=====================

Il est possible de modifier les civilites dans le menu parametrage -> civilite

.. image:: tab_civilite.png

En appuyant sur modification :

.. image:: form_civilite.png






