.. _rivoli:

######
Rivoli
######

Sert pour l'adressage automatique

Saisie des rivolis
==================

Il est possible de modifier les rivolis dans le menu paramétrage -> rivoli

.. image:: tab_rivoli.png

En appuyant sur modification :

.. image:: form_rivoli.png






Les adresses postales
=====================

Il est possible de visualiser les adresses postales pour le rivoli concerné
dans le sous formulaire adresse postale

.. image:: tab_adresse_postale_rivoli.png


