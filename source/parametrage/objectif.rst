.. _objectif:

########
Objectif
########

Saisie des objectifs
=====================

Il est possible de modifier les objectifs dans le menu paramétrage -> objectif

.. image:: tab_objectif.png

En appuyant sur modification :

.. image:: form_objectif.png






Les dossiers 
============

Il est possible de visualiser les dossiers pour l'objectif concerné
dans le sous formulaire critere_reperage

.. image:: tab_critere_reperage_objectif.png

Les evenements 
==============

Il est possible de visualiser les evenements pour l'objectif concerné
dans le sous formulaire evenement

.. image:: tab_evenement_objectif.png
