.. _type_etablissement:

##########################
Les types d'établissements
##########################

Saisie des type_etablissements
==============================

Il est possible de modifier les type_etablissements dans le menu paramétrage -> type_etablissement

.. image:: tab_type_etablissement.png

En appuyant sur modification :

.. image:: form_type_etablissement.png






Les établissements 
==================

Il est possible de visualiser les etablissements pour le type_etablissement concerné
dans le sous formulaire établissement

.. image:: tab_etablissement_type_etablissement.png


