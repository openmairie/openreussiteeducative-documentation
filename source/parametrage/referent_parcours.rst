.. _referent_parcours:

#################
Référent parcours
#################

Saisie des referent_parcours
============================

Il est possible de modifier les referents parcours dans le menu paramétrage -> referent_parcours

.. image:: tab_referent_parcours.png

En appuyant sur modification :

.. image:: form_referent_parcours.png





