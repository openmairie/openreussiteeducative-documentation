.. _niveau:

###############
Niveau scolaire
###############

Saisie des niveaux
==================

Il est possible de modifier les niveaus dans le menu parametrage -> niveau

.. image:: tab_niveau.png

En appuyant sur modification :

.. image:: form_niveau.png






Les dossiers
============

Il est possible de visualiser les dossiers pour le niveau concerné
dans le sous formulaire intervention

.. image:: tab_dossier_niveau.png


