.. _prescripteur:

############
Prescripteur
############

Saisie des prescripteurs
========================

Il est possible de modifier les prescripteurs dans le menu application -> prescripteur

.. image:: tab_prescripteur.png

En appuyant sur modification :

.. image:: form_prescripteur.png






Les dossiers
============

Il est possible de visualiser les dossiers pour le prescripteur concerné
dans le sous formulaire dossier

.. image:: tab_dossier_prescripteur.png


