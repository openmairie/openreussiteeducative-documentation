.. _parametrage:

###########
Paramétrage
###########


Il est proposer de décrire le paramétrage de l'application.




.. toctree::

    om_parametre.rst
    domaine.rst
    objectif.rst
    action.rst
    motif.rst
    civilite.rst
    situation_familiale.rst
    prescripteur.rst
    referent_familial.rst
    referent_parcours.rst
    etablissement.rst
    type_etablissement.rst
    niveau.rst
    rivoli.rst
    adresse_postale.rst
    quartier.rst
    perimetre.rst



